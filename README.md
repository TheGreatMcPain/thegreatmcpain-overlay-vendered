# Vendored distfiles for packages in the FeM overlay

This repository provides pipelines for automatically vendoring distfiles required for some packages in the [thegreatmcpain-overlay](https://gitlab.com/TheGreatMcPain/thegreatmcpain-overlay).

## Go vendor tarballs

To create vendor tarballs,
simply edit the CI configuration and add a new item to the `make_modcache` job matrix.
The variables `PN`, `PV`, `SRC_URI` and `S` must be specified in the same way as in the ebuild.
