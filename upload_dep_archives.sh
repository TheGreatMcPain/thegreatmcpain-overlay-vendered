#!/bin/bash

cd "$(dirname "${0}")"

: ${DEP_ARCHIVES_DIR:="dep-archives"}

info() {
	echo "[INFO] $*"
}

die() {
	echo "[ERROR] $*"
	exit 1
}

extract_pn_pv() {
	local separated_pn_pv="$(echo ${1} | sed 's/^\(.*\)-\(\([[:digit:]][[:digit:]]*\.\)*[[:digit:]][[:digit:]]*\)-deps.tar.*$/\1 \2/g')"
	PN="$(echo ${separated_pn_pv} | awk '{print $1;}')"
	PV="$(echo ${separated_pn_pv} | awk '{print $2;}')"
	P="${PN}-${PV}"
}

which curl 2>&1 || die "Failed to find curl executable"
which sed 2>&1 || die "Failed to find sed executable"
[[ "${CI_JOB_TOKEN}" == "" ]] && die "Empty CI_JOB_TOKEN"
[[ "${CI_API_V4_URL}" == "" ]] && die "Empty CI_API_V4_URL"
[[ "${CI_PROJECT_ID}" == "" ]] && die "Empty CI_PROJECT_ID"

pushd "${DEP_ARCHIVES_DIR}"
for archive in *-deps.tar*; do
	! [[ -f "${archive}" ]] && continue
	extract_pn_pv "${archive}"

	info "Uploading ${P} to package registry"

	curl \
		--header "JOB-TOKEN: $CI_JOB_TOKEN" \
		--upload-file "${archive}" \
		"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PN}/${PV}%2B$(date -I)/${archive}" \
		|| die "Failed to upload ${P}"
done
